const http = require('http');
const url = require('url');
const path = require('path');
const fs = require('fs');


const app = http.createServer((req, res) => {
  let pathname = url.parse(req.url).pathname
  console.log(pathname, 'name');
  if (pathname == "/") {
    pathname = "/index.html";
  }
  if (pathname != '/favicon.ico') {
    const extname = path.extname(pathname);
    fs.readFile(`./static${pathname}`, (err, files) => {
      if (err) {
        res.writeHead(404, { "Content-type": mime });
        res.end('404');
        return;
      }
      const mime = getMime(extname);
      res.writeHead(200, { "Content-type": mime });
      res.end(files);
    })
  }
})

function getMime(extname) {
  switch (extname) {
    case ".html":
      return "text/html";
    case ".jpg":
      return "image/jpg";
    case ".css":
      return "text/css";
    case ".js":
      return "application/javascript;charset=UTF-8";
    case ".json":
      return "json"
  }
}
app.listen(80, () => console.log('ok'))