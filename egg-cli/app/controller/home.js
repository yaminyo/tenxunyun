'use strict';
const fs = require('fs');
const { resolve } = require('path');
const Controller = require('egg').Controller;

class HomeController extends Controller {
  async index() {
    const { ctx } = this;
    ctx.response.type = 'html';
    const tpl = fs.readFileSync(resolve(__dirname, '../public/dist/index.html'), { encoding: 'utf-8' });
    ctx.body = tpl;
  }
}

module.exports = HomeController;
