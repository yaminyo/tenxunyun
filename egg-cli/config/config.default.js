/* eslint valid-jsdoc: "off" */

'use strict';

const path = require('path');
/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1618474156770_2263';

  // add your middleware config here
  config.middleware = [];
  // static
  config.static = {
    prefix: '/assets/',
    dir: [
      path.join(path.dirname('app/public/*')),
      path.join(path.dirname('app/public/dist/assets/*')),
    ],
    dynamic: true,
    preload: false,
    maxAge: 0,
    buffer: false,
  };
  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
  };


  return {
    ...config,
    ...userConfig,
  };
};
