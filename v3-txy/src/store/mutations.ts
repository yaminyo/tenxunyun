import { IToDo, IState, IMutations } from '../types';
import { ADD_TODO, DEL_TODO, EDIT_TODO, UPDATE_TODO } from './storeTypes';


const mutations: IMutations = {
  // 添加todo
  [ADD_TODO](state: IState, value: IToDo):void {
    state.todoList = [...state.todoList, value];
  },
  // 更新状态
  [UPDATE_TODO](state: IState, id: number):void {
    state.todoList = state.todoList.map((item):IToDo => {
      if (item.id === id) {
        item.status += 1;
      }
      return item;
    })
  },
  [EDIT_TODO](state, {id, type}: { id: number, type: string|undefined }): void {
    state.todoList = state.todoList.map((item) => {
      console.log(item, 'item');
      
      item.editChecked = false;
      if (item.id === id) {
        item.editChecked = true;
        if (type) {
          item.editChecked = false;
        }
        
      }
      return item;
    })
  },
  // 删除todo
  [DEL_TODO](state, id: number): void {
    state.todoList = state.todoList.filter((item) => item.id !== id);
  }
};

export default mutations;