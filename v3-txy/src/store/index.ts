import { createStore, Store } from 'vuex';
import state from './state';
import getters from './getters';
import mutations from './mutations';
import actions from './actions';
import persistedstate from './plugins/persistedstate';

const plugin  = persistedstate();

const store = createStore({
  state,
  getters,
  mutations,
  actions,
  plugins: [plugin]
})

export default store;