import { IState } from "../types";

const state: IState = {
  todoList: []
}

export default state;