import { IActions, IToDo } from "../types";
import { ADD_TODO, DEL_TODO, EDIT_TODO, UPDATE_TODO } from "./storeTypes";

const actions: IActions = {
  [ADD_TODO](ctx: any, todo: IToDo) {
    ctx.commit(ADD_TODO, todo)
  },
  [UPDATE_TODO](ctx: any, id: number) {
    ctx.commit(UPDATE_TODO, id)
  },
  [EDIT_TODO]({ commit }, {id, type}: { id: number, type: string|undefined }) {
    commit(EDIT_TODO, {id, type})
  },
  [DEL_TODO](ctx: any, id: number) {
    ctx.commit(DEL_TODO, id)
  }

}

export default actions;