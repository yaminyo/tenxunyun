import {
  ElInput,
  ElButton,
  ElTabs,
  ElTabPane,
  ElCheckbox,
  ElEmpty,
  ElMessage

} from 'element-plus';

const components  = [
  ElInput,
  ElButton,
  ElTabs,
  ElTabPane,
  ElCheckbox,
  ElEmpty,
  ElMessage
]

export default {
  components
}

