import { ElMessage } from 'element-plus';

const message = {
  $open2(msg) {
    ElMessage({
      showClose: true,
      message: msg,
      type: 'success'
    })
  },
  $open3(msg) {
    ElMessage({
      showClose: true,
      message: msg,
      type: 'warning'
    })
  }
}

export default message;