import { createRouter, createWebHashHistory } from 'vue-router';


import Todo from '../view/todo/index.vue';
import NotFound from '../view/notFound/notFound.vue';

const routes = [
  { path: '/', redirect: '/todo', component: Todo },
  { path: '/todo', component: Todo },
  { path: '/:pathMatch(.*)*', name: 'not-found', component: NotFound },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
})

export default router;