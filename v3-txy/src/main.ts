import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import ElementPlus from './utils/element';
import './assets/style/index.scss';
import message from './utils/index';

const app = createApp(App);
app.config.globalProperties = message;
// 引入element
ElementPlus.components.forEach((item) => {
  app.use(item);
})
app.use(router);
app.use(store);
app.mount('#app');
