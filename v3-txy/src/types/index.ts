import { ActionContext } from 'vuex';

export interface Itabs {
  status: string,
  label: string
}

export enum TODO_STUTAS {
  'unfinished' = 1,
  'underway' = 2,
  'complete' = 3,
}

export interface IToDo {
  id: number,
  value: string,
  checked?: boolean,
  status: number,
  editChecked: boolean
}
export interface IState {
  todoList: IToDo[]
}
export interface IMutations {
  [x: string]: (state: IState, value: any) => void
}

export interface IActions {
  [x: string]: (ctx: ActionContext<IState, any>, value:any) => void
}